package service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import repository.EmpleadoRepository;
import domain.Empleado;

@Service
public class EmpleadoService {

	@Autowired
	EmpleadoRepository empleadoRepository;

	@Transactional
	public void save(Empleado person) {
		if (person.getId() == null) {
			empleadoRepository.persist(person);
		} else {
			empleadoRepository.merge(person);
		}
	}

	public Empleado get(Long id) {
		return empleadoRepository.find(id);
	}

	public Collection<Empleado> getAll() {
		return empleadoRepository.findAll();
	}
}
