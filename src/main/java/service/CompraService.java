package service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import repository.CompraRepository;
import domain.Compra;

@Service
public class CompraService {

	@Autowired
	CompraRepository compraRepository;

	@Transactional
	public void save(Compra person) {
		if (person.getId() == null) {
			compraRepository.persist(person);
		} else {
			compraRepository.merge(person);
		}
	}

	public Compra get(Long id) {
		return compraRepository.find(id);
	}

	public Collection<Compra> getAll() {
		return compraRepository.findAll();
	}
}
