
package service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import repository.CategoriaProductoRepository;
import domain.CategoriaProducto;

@Service
public class CategoriaProductoService {

	@Autowired
	CategoriaProductoRepository compraRepository;

	@Transactional
	public void save(CategoriaProducto person) {
		if (person.getId() == null) {
			compraRepository.persist(person);
		} else {
			compraRepository.merge(person);
		}
	}

	public CategoriaProducto get(Long id) {
		return compraRepository.find(id);
	}

	public Collection<CategoriaProducto> getAll() {
		return compraRepository.findAll();
	}
}
