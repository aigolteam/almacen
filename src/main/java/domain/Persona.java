package domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;


/**
 * The persistent class for the persona database table.
 * 
 */
@Entity
@Table(name="persona")
@NamedQuery(name="Persona.findAll", query="SELECT p FROM Persona p")
public class Persona implements Serializable, domain.BaseEntity<Long> {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="person_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long personId;

	
	
	@Column(name="address_1")
	private String address1;

	@Column(name="address_2")
	private String address2;

	private String city;

	@Column(columnDefinition = "TEXT")
	private String comments;


	//@Column(columnDefinition = "TEXT")
	private String email;

	@Column(name="first_name")
	private String firstName;

	@Column(name="last_name")
	private String lastName;

	
	/*
	 * un
	 * */
	  @OneToMany(mappedBy="empleado" )
	  private List<Empleado> empleado;
	
	
	/*
	 * referencia a proveedor 
	 * */
	  @OneToMany
	  @JoinColumn(name="person_id", referencedColumnName="person_id")
	  private List<Proveedor> proveedor;
	
	  

	@Column(name="phone_number")
	private String phoneNumber;

	
	private String state;

	public Persona() {
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public Long getId() {
		return personId;
		
	}

	@Override
	public void setId(Long id) {
		personId=id;
	}

}