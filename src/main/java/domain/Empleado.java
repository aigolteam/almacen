package domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the empleado database table.
 * 
 */
@Entity
@Table(name="empleado")
@NamedQuery(name="Empleado.findAll", query="SELECT e FROM Empleado e")
public class Empleado implements Serializable, domain.BaseEntity<Long> {
	
	private static final long serialVersionUID = 1L;

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private int deleted;
	
	@Column(columnDefinition = "TEXT")
	private String password;

	
	//@Column(name="person_id")
	//@ManyToOne
	//@JoinColumn(name="person_id")
	//private int person_id;

	
	 @ManyToOne(fetch=FetchType.LAZY)
	  @JoinColumn(name="person_id" ,foreignKey = @ForeignKey(name = "fk_emp_persona"))
	  private Persona empleado;
	

	
	
	
	private String username;

	public Empleado() {
	}

	public int getDeleted() {
		return this.deleted;
	}

	public void setDeleted(int deleted) {
		this.deleted = deleted;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	/*public int getPersonId() {
		return this.person_id;
	}

	public void setPersonId(int personId) {
		this.person_id = personId;
	}
*/
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setId(Long id) {
		// TODO Auto-generated method stub
		
	}

}