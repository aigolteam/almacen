package domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;


/**
 * The persistent class for the categoria_producto database table.
 * 
 */
@Entity
@Table(name="categoria_general_producto")
@NamedQuery(name="CategoriaGeneralProducto.findAll", query="SELECT c FROM CategoriaGeneralProducto c")
public class CategoriaGeneralProducto implements Serializable, domain.BaseEntity<Long> {
	private static final long serialVersionUID = 1L;

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	@Column(name="categoria_gproducto_id")
	private Long id;
	
	@Column(length=45)
	private String nombre;

	
	 @ManyToMany
	  @JoinTable(
	      name="categoria_producto_has_categoria_general_producto",
	      joinColumns={@JoinColumn(name="categoria_gproducto_id", referencedColumnName="categoria_gproducto_id")},
	      inverseJoinColumns={@JoinColumn(name="categoria_producto_id", referencedColumnName="id")})
	 private List<CategoriaProducto> cproducto;
	
	public CategoriaGeneralProducto() {
	}

	public String getNombre() {
		return this.nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public Long getId() {
	
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id=id;
		
	}

}