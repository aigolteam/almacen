package domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;


/**
 * The persistent class for the categoria_producto database table.
 * 
 */
@Entity
@Table(name="categoria_producto")
@NamedQuery(name="CategoriaProducto.findAll", query="SELECT c FROM CategoriaProducto c")
public class CategoriaProducto implements Serializable, domain.BaseEntity<Long> {
	private static final long serialVersionUID = 1L;

	private boolean deleted;

	@Column(columnDefinition = "TEXT")
	private String descripcion;

	private int descuento;

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	private Long id;
	@Column(columnDefinition = "TEXT")
	private String nombre;

	
	
	/*
	 * una categoria tiene varios productos
	 * */
	  @OneToMany
	  @JoinColumn(name="category", referencedColumnName="id")
	  private List<Producto> categoria;
	
	public CategoriaProducto() {
	}

	public boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getDescuento() {
		return this.descuento;
	}

	public void setDescuento(int descuento) {
		this.descuento = descuento;
	}


	@Column(columnDefinition = "TEXT")
	public String getNombre() {
		return this.nombre;
	}
	@Column(columnDefinition = "TEXT")
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public Long getId() {
	
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id=id;
		
	}

}