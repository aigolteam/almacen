package domain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;
import java.math.BigDecimal;


/**
 * The persistent class for the producto database table.
 * 
 */
@Entity
@Table(name="producto")
@NamedQuery(name="Producto.findAll", query="SELECT p FROM Producto p")
public class Producto implements Serializable, BaseEntity<Long> {
	private static final long serialVersionUID = 1L;

	private Long category;

	@Column(name="codigo_asignado")
	private String codigoAsignado;

	private int deleted;

	private String description;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	@Column(name="item_id")
	private Long itemId;

	
	
	/*
	 * varios productos de una categoria
	 * 
	 * */
		@ManyToOne(fetch=FetchType.LAZY)
		@JoinColumn(name="category" , referencedColumnName="id",foreignKey = @ForeignKey(name = "fk_prod_cate") , insertable = false, updatable = false)
		private CategoriaProducto categoria;
	
	
		
	private String name;

	@Column(name="supplier_id")
	private Long supplierId;

	/*
	 * varios proveedores de una producto
	 * 
	 * */
		@ManyToOne(fetch=FetchType.LAZY)
		@JoinColumn(name="supplier_id" , referencedColumnName="person_id",foreignKey = @ForeignKey(name = "fk_prod_prov") , insertable = false, updatable = false)
		private Proveedor proveedor;
	
		
		/*
		 * un producto en varios almacenes
		 * 
		 * */
		  @OneToMany
		  @JoinColumn(name="item_id", referencedColumnName="item_id")
		  private List<AlmProd> almacenes;
		
	
	@Column(name="tiempo_llegada")
	private Timestamp tiempoLlegada;

	@Column(name="unit_price")
	private BigDecimal unitPrice;

	
	
	
	
	public Producto() {
	}

	public Long getCategory() {
		return this.category;
	}

	public void setCategory(Long category) {
		this.category = category;
	}

	public String getCodigoAsignado() {
		return this.codigoAsignado;
	}

	public void setCodigoAsignado(String codigoAsignado) {
		this.codigoAsignado = codigoAsignado;
	}

	public int getDeleted() {
		return this.deleted;
	}

	public void setDeleted(int deleted) {
		this.deleted = deleted;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getItemId() {
		return this.itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getSupplierId() {
		return this.supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public Timestamp getTiempoLlegada() {
		return this.tiempoLlegada;
	}

	public void setTiempoLlegada(Timestamp tiempoLlegada) {
		this.tiempoLlegada = tiempoLlegada;
	}

	public BigDecimal getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setId(Long id) {
		// TODO Auto-generated method stub
		
	}

}