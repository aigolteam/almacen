package domain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigInteger;
import java.util.List;
import java.math.BigDecimal;


/**
 * The persistent class for the alm_prod database table.
 * 
 */
@Entity
@Table(name="alm_prod")
@IdClass(AlmProdPK.class)
@NamedQuery(name="AlmProd.findAll", query="SELECT a FROM AlmProd a")
public class AlmProd implements Serializable, BaseEntity<Long> {
	private static final long serialVersionUID = 1L;

	private Long codigo;

	private byte deleted;

	@Id
	@Column(name="idAlmacen")
	private Long idAlmacen;

	@Id
	@Column(name="item_id")
	private Long itemId;

	private BigDecimal precio;

	
	/*
	 * una Almacen tiene varios a travez de alm_id  productos
	 * */

		@ManyToOne(fetch=FetchType.LAZY)
		@JoinColumn(name="idAlmacen" , referencedColumnName="idAlmacen",foreignKey = @ForeignKey(name = "fk_prod_alm_1") , insertable = false, updatable = false)
		  private Almacen almacen;
		
	  
		
		
		@ManyToOne(fetch=FetchType.LAZY)
		@JoinColumn(name="item_id" , referencedColumnName="item_id",foreignKey = @ForeignKey(name = "fk_prod_alm_2") , insertable = false, updatable = false)
		  private Producto producto;
		
	public AlmProd() {
	}

	public Long getCodigo() {
		return this.codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public byte getDeleted() {
		return this.deleted;
	}

	public void setDeleted(byte deleted) {
		this.deleted = deleted;
	}

	public Long getIdAlmacen() {
		return this.idAlmacen;
	}

	public void setIdAlmacen(Long idAlmacen) {
		this.idAlmacen = idAlmacen;
	}

	public Long getItemId() {
		return this.itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public BigDecimal getPrecio() {
		return this.precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setId(Long id) {
		// TODO Auto-generated method stub
		
	}

}