package domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the transferencias database table.
 * 
 */
@Entity
@Table(name="transferencias")
@NamedQuery(name="Transferencia.findAll", query="SELECT t FROM Transferencia t")
public class Transferencia implements Serializable, BaseEntity<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	private Long idTransferencia;
	
	@Column(columnDefinition = "TEXT")
	private String descripcion;

	private Timestamp fechaconfirmacion;

	private Timestamp fechaenvio;

	private Long idalmacen;

	private Long idalmaceno;

	private int idcajar;

	private Long idempleado;

	private Long idempleador;



	private byte recibido;

	
	
	 @ManyToMany
	  @JoinTable(
	      name="transES",
	      joinColumns={@JoinColumn(name="idtrans", referencedColumnName="idTransferencia")},
	      inverseJoinColumns={@JoinColumn(name="codigoES", referencedColumnName="codigo")})
	 private List<EntradaSalida> entradasSalidas;
	 
	public Transferencia() {
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Timestamp getFechaconfirmacion() {
		return this.fechaconfirmacion;
	}

	public void setFechaconfirmacion(Timestamp fechaconfirmacion) {
		this.fechaconfirmacion = fechaconfirmacion;
	}

	public Timestamp getFechaenvio() {
		return this.fechaenvio;
	}

	public void setFechaenvio(Timestamp fechaenvio) {
		this.fechaenvio = fechaenvio;
	}

	public Long getIdalmacen() {
		return this.idalmacen;
	}

	public void setIdalmacen(Long idalmacen) {
		this.idalmacen = idalmacen;
	}

	public Long getIdalmaceno() {
		return this.idalmaceno;
	}

	public void setIdalmaceno(Long idalmaceno) {
		this.idalmaceno = idalmaceno;
	}

	public int getIdcajar() {
		return this.idcajar;
	}

	public void setIdcajar(int idcajar) {
		this.idcajar = idcajar;
	}

	public Long getIdempleado() {
		return this.idempleado;
	}

	public void setIdempleado(Long idempleado) {
		this.idempleado = idempleado;
	}

	public Long getIdempleador() {
		return this.idempleador;
	}

	public void setIdempleador(Long idempleador) {
		this.idempleador = idempleador;
	}

	public Long getIdTransferencia() {
		return this.idTransferencia;
	}

	public void setIdTransferencia(Long idTransferencia) {
		this.idTransferencia = idTransferencia;
	}

	public byte getRecibido() {
		return this.recibido;
	}

	public void setRecibido(byte recibido) {
		this.recibido = recibido;
	}

	@Override
	public java.lang.Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setId(java.lang.Long id) {
		// TODO Auto-generated method stub
		
	}

}