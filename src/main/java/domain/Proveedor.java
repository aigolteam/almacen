package domain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigInteger;
import java.util.List;


/**
 * The persistent class for the proveedor database table.
 * 
 */
@Entity
@Table(name="proveedor")
@NamedQuery(name="Proveedor.findAll", query="SELECT p FROM Proveedor p")
public class Proveedor implements Serializable, domain.BaseEntity<Long> {
	private static final long serialVersionUID = 1L;

	@Column(name="company_name")
	private String companyName;

	private int deleted;

	@Id
	@Column(name="person_id")
	private Long personId;

	@Column(name="RUC")
	private String ruc;

/*
 * columna join proveedor.person_id=persona.person_id
 * */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="person_id" , referencedColumnName="person_id",foreignKey = @ForeignKey(name = "fk_prov_persona") , insertable = false, updatable = false)
	private Persona proveedor;
	
	 
	 
	 
	 
	//@OneToMany
	//  @JoinColumn(name="proveedor_id)")
	  	 @OneToMany(mappedBy="pedido" )
		  private List<Pedido> pedido;

	  	 
	/*
		 * una proveedor tiene varios productos
		 * */
		  @OneToMany
		  @JoinColumn(name="supplier_id", referencedColumnName="person_id")
		  private List<Producto> productos;
	public Proveedor() {
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public int getDeleted() {
		return this.deleted;
	}

	public void setDeleted(int deleted) {
		this.deleted = deleted;
	}

	public Persona getPersonId() {
		return this.proveedor;
	}

	public void setPersonId(Persona personId) {
		this.proveedor = personId;
	}

	public String getRuc() {
		return this.ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setId(Long id) {
		// TODO Auto-generated method stub
		
	}

}