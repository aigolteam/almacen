package domain;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;


/**
 * The persistent class for the view_saldo database table.
 * 
 */
@Entity
@Table(name="view_saldo")
@NamedQuery(name="ViewSaldo.findAll", query="SELECT v FROM ViewSaldo v")
public class ViewSaldo implements Serializable, BaseEntity<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idsaldo;
	private BigDecimal cantidad;

	
	private Long codigo;

	private Long idalmacen;

	private Long idProducto;

	


	private BigDecimal saldocantidad;

	public ViewSaldo() {
	}

	public BigDecimal getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public Long getCodigo() {
		return this.codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public Long getIdalmacen() {
		return this.idalmacen;
	}

	public void setIdalmacen(Long idalmacen) {
		this.idalmacen = idalmacen;
	}

	public Long getIdProducto() {
		return this.idProducto;
	}

	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}

	public Long getIdsaldo() {
		return this.idsaldo;
	}

	public void setIdsaldo(Long idsaldo) {
		this.idsaldo = idsaldo;
	}

	public BigDecimal getSaldocantidad() {
		return this.saldocantidad;
	}

	public void setSaldocantidad(BigDecimal saldocantidad) {
		this.saldocantidad = saldocantidad;
	}

	@Override
	public java.lang.Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setId(java.lang.Long id) {
		// TODO Auto-generated method stub
		
	}

}