/*package domain;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


 /**
 * The persistent class for the compra database table.
 * 
 */ /*
@Entity
@Table(name="compra")
public class Compra implements BaseEntity<Long> {
	@Id
	//@SequenceGenerator(name = "person_id_generator", sequenceName = "person_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;

	@Column(length = 64)
	private String firstName;

	@Column(length = 64)
	private String lastName;


	@Override
	public Long getId() {
		return codigo;
	}

	@Override
	public void setId(Long id) {
		this.codigo = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



}

*/








package domain;


import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;




/**
 * The persistent class for the compra database table.
 * */
 
@Entity
@Table(name="compra")
@NamedQuery(name="Compra.findAll", query="SELECT c FROM Compra c")
public class Compra implements Serializable, domain.BaseEntity<Long> {
	private static final long serialVersionUID = 1L;

	@Id
	//@SequenceGenerator(name = "person_id_generator", sequenceName = "person_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;
	@Column(columnDefinition = "TEXT")
	private String detalle;

	private Timestamp fechaCompra;

	private int idcompra;

	private int idproveedor;
	
	@Column(columnDefinition = "TEXT")
	private String nombreFact;

	private String numero;

	private BigDecimal precio;

	private String serie;

	public Compra() {
	}




	public String getDetalle() {
		return this.detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public Timestamp getFechaCompra() {
		return this.fechaCompra;
	}

	public void setFechaCompra(Timestamp fechaCompra) {
		this.fechaCompra = fechaCompra;
	}

	public int getIdcompra() {
		return this.idcompra;
	}

	public void setIdcompra(int idcompra) {
		this.idcompra = idcompra;
	}

	public int getIdproveedor() {
		return this.idproveedor;
	}

	public void setIdproveedor(int idproveedor) {
		this.idproveedor = idproveedor;
	}

	public Object getNombreFact() {
		return this.nombreFact;
	}

	public void setNombreFact(String nombreFact) {
		this.nombreFact = nombreFact;
	}

	public String getNumero() {
		return this.numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public BigDecimal getPrecio() {
		return this.precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public String getSerie() {
		return this.serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	@Override
	public Long getId() {
		
		return codigo;
	}

	@Override
	public void setId(Long id) {
	codigo=id;
		
	}

}