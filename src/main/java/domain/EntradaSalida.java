package domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.math.BigDecimal;


/**
 * The persistent class for the EntradaSalida database table.
 * 
 */
@Entity
@NamedQuery(name="EntradaSalida.findAll", query="SELECT e FROM EntradaSalida e")
public class EntradaSalida implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	private Long codigo;

	private BigDecimal cantidad;

	private BigDecimal costo;

	private Timestamp fecha;

	private Long idalmacen;

	private Long idcaja;

	private Long idEmpleado;

	private Long idProducto;

	private byte isEntrada;

	private String numeroComprobante;

	private BigDecimal saldocantidad;

	@Column(columnDefinition = "TEXT")
	private String tipoComprobante;

	private Long tipoOperacion;

	public EntradaSalida() {
	}

	public BigDecimal getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public Long getCodigo() {
		return this.codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public BigDecimal getCosto() {
		return this.costo;
	}

	public void setCosto(BigDecimal costo) {
		this.costo = costo;
	}

	public Timestamp getFecha() {
		return this.fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public Long getIdalmacen() {
		return this.idalmacen;
	}

	public void setIdalmacen(Long idalmacen) {
		this.idalmacen = idalmacen;
	}

	public Long getIdcaja() {
		return this.idcaja;
	}

	public void setIdcaja(Long idcaja) {
		this.idcaja = idcaja;
	}

	public Long getIdEmpleado() {
		return this.idEmpleado;
	}

	public void setIdEmpleado(Long idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public Long getIdProducto() {
		return this.idProducto;
	}

	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}

	public byte getIsEntrada() {
		return this.isEntrada;
	}

	public void setIsEntrada(byte isEntrada) {
		this.isEntrada = isEntrada;
	}

	public String getNumeroComprobante() {
		return this.numeroComprobante;
	}

	public void setNumeroComprobante(String numeroComprobante) {
		this.numeroComprobante = numeroComprobante;
	}

	public BigDecimal getSaldocantidad() {
		return this.saldocantidad;
	}

	public void setSaldocantidad(BigDecimal saldocantidad) {
		this.saldocantidad = saldocantidad;
	}

	public String getTipoComprobante() {
		return this.tipoComprobante;
	}

	public void setTipoComprobante(String tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}

	public Long getTipoOperacion() {
		return this.tipoOperacion;
	}

	public void setTipoOperacion(Long tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

}