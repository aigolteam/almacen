package domain;

import javax.persistence.*;
import java.io.Serializable;

public class AlmProdPK implements Serializable {

    private Long idAlmacen;

    private Long itemId;
    
    public AlmProdPK(){}
    public AlmProdPK(Long idAlmacen, Long item_id) {
        this.idAlmacen   = idAlmacen;
        this.itemId    = item_id;
    }

    public boolean equals(Object object) {
        if (object instanceof AlmProdPK) {
        	AlmProdPK pk = (AlmProdPK)object;
            return idAlmacen == pk.idAlmacen && itemId == pk.itemId;
        } else {
            return false;
        }
    }

    public int hashCode() {
        return (int)(idAlmacen + itemId );
    }
}