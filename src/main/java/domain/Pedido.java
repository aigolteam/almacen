package domain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigInteger;
import java.sql.Timestamp;


/**
 * The persistent class for the pedido database table.
 * 
 */
@Entity
@Table(name="pedido")
@NamedQuery(name="Pedido.findAll", query="SELECT p FROM Pedido p")
public class Pedido implements Serializable, BaseEntity<Long> {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idpedido;

	private byte estado;

	@Column(name="fecha_pedido")
	private Timestamp fechaPedido;

	@Column(name="fecha_recepcion")
	private Timestamp fechaRecepcion;


	
	/*
	 * varios pedidos de un proveedor
	 * */
	 @ManyToOne(fetch=FetchType.LAZY)
	  @JoinColumn(name="proveedor_id" ,foreignKey = @ForeignKey(name = "fk_prov_proveedor"))
	  private Proveedor pedido;
		
	
	
	public Pedido() {
	}

	public byte getEstado() {
		return this.estado;
	}

	public void setEstado(byte estado) {
		this.estado = estado;
	}

	public Timestamp getFechaPedido() {
		return this.fechaPedido;
	}

	public void setFechaPedido(Timestamp fechaPedido) {
		this.fechaPedido = fechaPedido;
	}

	public Timestamp getFechaRecepcion() {
		return this.fechaRecepcion;
	}

	public void setFechaRecepcion(Timestamp fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}

	public int getIdpedido() {
		return this.idpedido;
	}

	public void setIdpedido(int idpedido) {
		this.idpedido = idpedido;
	}
/*
	public BigInteger getProveedorId() {
		return this.proveedorId;
	}

	public void setProveedorId(BigInteger proveedorId) {
		this.proveedorId = proveedorId;
	}
*/
	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setId(Long id) {
		// TODO Auto-generated method stub
		
	}

}