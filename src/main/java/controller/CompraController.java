package controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import service.CompraService;
import domain.Compra;


@Controller
public class CompraController {
	
	@Autowired
	CompraService compraService;

	@RequestMapping(value = "/compra", method = RequestMethod.POST)
	String savePerson(@ModelAttribute Compra person, ModelMap model) {
		System.out.println("saving: " + person.getId());
		compraService.save(person);
		return showPerson(person.getId(), model);
	}
	@RequestMapping(value = "/add-compra", method = RequestMethod.GET)
	String addNewPerson(@RequestParam(required = false) Long id,  ModelMap model) {
		Compra person = id == null ? new Compra() : compraService.get(id);
		model.addAttribute("compra", person);
		if(id != null)
		System.out.println("puso en modify add compra "+id.toString());
		
		return "add-compra";
		
	}

	@RequestMapping(value = "/compra", method = RequestMethod.GET)
	String showPerson(@RequestParam(required = false) Long id, ModelMap model) {
		if (id != null) {
			System.out.println("puso en showperson "+id.toString());
			Compra person = compraService.get(id);
		if(person!=null)
				System.out.println("serie: "+person.getSerie().toString());
			
			model.addAttribute("compra", person);
			return "compra";
		} else {
			Collection<Compra> people = compraService.getAll();
			model.addAttribute("people", people);
			return "compras";
		}
	}

}
