package controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import service.AccountService;
import service.EmpleadoService;
import service.TransferService;
import domain.Account;
import domain.Empleado;
import domain.Person;
import form.CreateAccountForm;
import form.TransferForm;

@Controller
public class Empleado_Controller {

/*	@Autowired
	TransferService transferService;

	@Autowired
	AccountService accountService;*/

	@Autowired
	EmpleadoService Empleadoservice;
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	String Autenticacion(@RequestParam(required = false) Long usuario, ModelMap model) {
		if (usuario == null  ) {
			return "login";
		} else {

			System.out.println("inserto usuario "+usuario.toString());
			Empleado person = Empleadoservice.get(usuario);
			if(person!=null)
			{
				model.addAttribute("some_var", "manson.twig.html");
				
				System.out.println("si existe "+person.getId());
				return "home";
				}
			else
			{
				System.out.println("no existe");
				return "login";
			}

		}
	}

	/*@RequestMapping(value = "/transfer", method = RequestMethod.GET)
	String showTransfer(@ModelAttribute TransferForm transfer, ModelMap model) {
		return "transfer";
	}

	@RequestMapping(value = "/account", method = RequestMethod.POST)
	String saveAccount(@ModelAttribute Account account, ModelMap model) {
		accountService.save(account);
		return "account-list";
	}

	@RequestMapping(value = "/account", method = RequestMethod.GET)
	String listAccounts(ModelMap model) {
		model.addAttribute("accounts", accountService.getAccounts());
		return "account-list";
	}

	@RequestMapping(value = "/add-account", method = RequestMethod.GET)
	String addAccount(ModelMap model) {
		return "add-account";
	}
	
	@RequestMapping(value = "/register-account", method = RequestMethod.POST)
	String createAccount(@ModelAttribute CreateAccountForm createAccount, ModelMap model) {
		accountService.createAccount(createAccount.getOwnerIds(), createAccount.getAccount());
		return "add-account";
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	String home(ModelMap model) {
		return "home";
	}*/
}
