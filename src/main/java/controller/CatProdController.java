package controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import service.CategoriaProductoService;
import domain.CategoriaProducto;


@Controller

public class CatProdController {
	
	@Autowired
	CategoriaProductoService compraService;

	@RequestMapping(value = "/comprap", method = RequestMethod.POST)
	String savePerson(@ModelAttribute CategoriaProducto person, ModelMap model) {
		System.out.println("saving: " + person.getId());
		compraService.save(person);
		return showPerson(person.getId(), model);
	}
	@RequestMapping(value = "/add-cat-prod", method = RequestMethod.GET)
	String addNewPerson(@RequestParam(required = false) Long id,  ModelMap model) {
		CategoriaProducto person = id == null ? new CategoriaProducto() : compraService.get(id);
		model.addAttribute("compra", person);
		if(id != null)
		System.out.println("puso en modify add compra "+id.toString());
		
		return "add-cat-prod";
		
	}

	@RequestMapping(value = "/comprap", method = RequestMethod.GET)
	String showPerson(@RequestParam(required = false) Long id, ModelMap model) {
		if (id != null) {
			System.out.println("puso en showperson "+id.toString());
			CategoriaProducto person = compraService.get(id);
		if(person!=null)
				System.out.println("serie: "+person.getNombre());
			
			model.addAttribute("compra", person);
			return "comprap";
		} else {
			Collection<CategoriaProducto> people = compraService.getAll();
			model.addAttribute("people", people);
			return "comprap";
		}
	}

}
