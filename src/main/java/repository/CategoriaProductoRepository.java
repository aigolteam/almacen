package repository;

import java.util.Collection;

import domain.CategoriaProducto;

public interface CategoriaProductoRepository extends BaseRepository<CategoriaProducto, Long> {

}
