
package repository.jpa;

import java.util.Collection;

import org.springframework.stereotype.Repository;

import repository.CategoriaProductoRepository;
import domain.CategoriaProducto;

@Repository
public class jpaCategoriaProducto extends JpaBaseRepository<CategoriaProducto, Long> implements
CategoriaProductoRepository {

}
