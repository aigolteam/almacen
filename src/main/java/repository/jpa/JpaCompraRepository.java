package repository.jpa;

import org.springframework.stereotype.Repository;

import repository.CompraRepository;
import domain.Compra;

@Repository
public class JpaCompraRepository extends JpaBaseRepository<Compra, Long> implements
		CompraRepository {
}
