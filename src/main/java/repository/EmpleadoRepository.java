package repository;

import java.util.Collection;


import domain.Empleado;

public interface EmpleadoRepository extends BaseRepository<Empleado, Long>{
	
	Empleado findByNumber(String number);

	Collection<Empleado> findByPersonId(Long personId);
	
}
